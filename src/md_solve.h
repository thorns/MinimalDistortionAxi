/*
 * Quasimaximal slicing -- actual solver code
 * Copyright (C) 2016 Anton Khirnov <anton@khirnov.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MD_SOLVE_H
#define MD_SOLVE_H

#include "common.h"

#include "cctk.h"

#include "basis.h"
#include "threadpool.h"

typedef struct MDSolverPriv MDSolverPriv;

typedef struct MDSolver {
    MDSolverPriv *priv;

    unsigned int nb_equations;

    MDBasisSetContext *basis[2][2];

    int nb_coeffs[2];
    int nb_colloc_points[2];

    double *coeffs;

    ThreadPoolContext *tp;
} MDSolver;

int md_solver_init(MDSolver **ctx,
                   cGH *cctkGH, ThreadPoolContext *tp,
                   unsigned int nb_equations,
                   unsigned int (*basis_order)[2],
                   double sf, double filter_power, double input_filter_power);

void md_solver_free(MDSolver **ctx);

int md_solver_solve(MDSolver *ctx);

void md_solver_print_stats(MDSolver *ctx);

#endif /* MD_SOLVE_H */

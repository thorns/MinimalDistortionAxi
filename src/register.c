void minimal_distortion_axi_register_mol(CCTK_ARGUMENTS)
{
    MoLRegisterConstrained(CCTK_VarIndex("ML_BSSN::beta1"));
    MoLRegisterConstrained(CCTK_VarIndex("ML_BSSN::beta2"));
    MoLRegisterConstrained(CCTK_VarIndex("ML_BSSN::beta3"));
}


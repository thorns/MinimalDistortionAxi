#ifndef MD_MD_H
#define MD_MD_H

#include "common.h"

#if HAVE_OPENCL
#include <cl.h>
#endif

#include <inttypes.h>

#include "cctk.h"

#include "md_solve.h"
#include "threadpool.h"

typedef struct MDContext MDContext;

#endif /* MD_MD_H */

/*
 * Basis sets for pseudospectral methods
 * Copyright (C) 2016 Anton Khirnov <anton@khirnov.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MD_BASIS_H
#define MD_BASIS_H

enum MDBasisEvalType {
    MD_BASIS_EVAL_TYPE_VALUE,
    MD_BASIS_EVAL_TYPE_DIFF1,
    MD_BASIS_EVAL_TYPE_DIFF2,
};

enum MDBasisFamily {
    MD_BASIS_FAMILY_TB_EVEN,
    MD_BASIS_FAMILY_SB_EVEN,
    MD_BASIS_FAMILY_SB_ODD,
    MD_BASIS_FAMILY_COS_EVEN,
};

typedef struct MDBasisSetContext MDBasisSetContext;

int md_basis_init(MDBasisSetContext **ctx, enum MDBasisFamily family, double sf);
void md_basis_free(MDBasisSetContext **ctx);

double md_basis_eval(const MDBasisSetContext *ctx, enum MDBasisEvalType type,
                     double coord, unsigned int order);
double md_basis_colloc_point(const MDBasisSetContext *ctx, unsigned int order,
                             unsigned int idx);

#endif /* MD_BASIS_H */
